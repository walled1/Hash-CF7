<div class="wrap">
    <h2><?php _e('Registered Form Submissions', $this->plugin_text_domain); ?></h2>
    <div id="nds-wp-list-table-demo">
        <div id="nds-post-body">
            <form method="get">
                <input type="hidden" name="page" value="connect-dedit-api" />
                <?php $this->user_list_table->search_box(__('search', $this->plugin_text_domain), 'search_id'); ?>
            </form>
        </div>
    </div>
</div>