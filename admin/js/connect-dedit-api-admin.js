jQuery(document).ready(function () {
  //handle the download process
  jQuery(document).on("click", "#download-dedit-record", function (e) {
    e.preventDefault();
    var record_id = jQuery(this).data("id"),
      download_record =
        "&action=dedit_action&param=download_dedit_record&id=" +
        record_id +
        "&security=" +
        dedit_connect_ajax_admin.ajax_nonce;
    jQuery.post(dedit_connect_ajax_admin.ajaxurl, download_record, function (
      response
    ) {
      //handle the response to be downloaded as json file
      var data = response.slice(0, -1),
        user_data = JSON.parse(data);
      const file = new Blob([user_data.download.formData], {
        type: "application/json",
      });
      const fileURL = URL.createObjectURL(file);
      // create the link
      const linkElement = document.createElement("a");

      // add the file url
      linkElement.setAttribute("href", fileURL);

      var fileName = JSON.parse(user_data.download.formData);

      // add the download attribute with name suggestion
      linkElement.setAttribute("download", user_data.file_name); //user_data.hashed_file
      //set id for this link
      linkElement.setAttribute("id", "download-dedit-record-file");
      jQuery(".actions").append(linkElement);
      //do the click event on this element
      linkElement.click();
    });
  });
  //handle the delete action process
  jQuery(document).on("click", "#delete-dedit-record", function (e) {
    e.preventDefault();
    var conf = confirm(dedit_connect_ajax_admin.delete_notice);
    if (conf) {
      var record_id = jQuery(this).data("id"),
        delete_record =
          "&action=dedit_action&param=delete_dedit_record&id=" +
          record_id +
          "&security=" +
          dedit_connect_ajax_admin.ajax_nonce;
      jQuery.post(dedit_connect_ajax_admin.ajaxurl, delete_record, function (
        response
      ) {
        var siteUrl = response.replace("0", "");
        var url = jQuery.parseJSON(siteUrl);
        window.location.href = url.admin_url + "&deleted_user=success";
      });
    }
  });
  //Bulk action
  jQuery(document).on("submit", "#wpse-list-table-form", function (e) {
    e.preventDefault();
    var conf = confirm(dedit_connect_ajax_admin.bulk_action_notice);
    if (conf) {
      var bulk_form = jQuery(this).serialize().replaceAll("%5B%5D", ""); //trim the form
      bulk_form +=
        "&action=dedit_action&param=bulk_delete_users" +
        "&security=" +
        dedit_connect_ajax_admin.ajax_nonce;
      jQuery.post(dedit_connect_ajax_admin.ajaxurl, bulk_form, function (
        response
      ) {
        var siteUrl = response.replace("0", "");
        var url = jQuery.parseJSON(siteUrl);
        window.location.href = url.admin_url + "&deleted_users=success";
      });
    }
  });
});
