<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://webw.us
 * @since      1.0.0
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/admin
 * @author     We Build Websites LLC <sales@webw.me>
 */
//check if the class exists or not

class Connect_Dedit_Api_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	private $table_activator;
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		require_once CONNECT_dedit_API_PATH . 'includes/class-connect-dedit-api-activator.php';
		$activator = new Connect_Dedit_Api_Activator();
		$this->table_activator = $activator;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{
		wp_enqueue_style($this->plugin_name, CONNECT_dedit_API_URL . 'admin/css/connect-dedit-api-admin.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_script('jquery');
		if ($_REQUEST['page'] === 'connect-dedit-api') {
			wp_enqueue_script('connect-dedit-admin-js', CONNECT_dedit_API_URL . 'admin/js/connect-dedit-api-admin.js', array('jquery'), $this->version, false);
			wp_localize_script('connect-dedit-admin-js', 'dedit_connect_ajax_admin', array(
				'name' => "Dedit.io Admin",
				'ajaxurl' => admin_url("admin-ajax.php"),
				'ajax_nonce' => wp_create_nonce('dedit-connect-admin'),
				'delete_notice' => __("Are you sure to delete this record?", $this->plugin_text_domain),
				'bulk_action_notice' =>  __("Are you sure to delete this records?", $this->plugin_text_domain)
			));
		}
	}
	//Add Fields into setting page from which the admin could enter his dedit.io information
	public function dedit_information_fields()
	{
		echo '<div class="wrap">
	<h1>' . __('Dedit.io Information Key', $this->plugin_text_domain) . '</h1>
	<form method="post" action="options.php">';

		settings_fields('dedit_settings'); // settings group name
		do_settings_sections('connect-dedit'); // just a page slug
		submit_button();

		echo '</form></div>';
	}
	public function dedit_textbox_callback()
	{
		register_setting(
			'dedit_settings', // settings group name
			'dedit_api_key', // option name
			'sanitize_text_field' // sanitization function
		);
		register_setting(
			'dedit_settings', // settings group name
			'dedit_blockchain', // option name
			'sanitize_text_field' // sanitization function
		);

		add_settings_section(
			'dedit_settings_section_id', // section ID
			'', // title (if needed)
			'', // callback function (if needed)
			'connect-dedit' // page slug
		);

		add_settings_field(
			'dedit_api_key',
			'dedit.io API Key',
			array($this, 'dedit_text_field_html'), // function which prints the field
			'connect-dedit', // page slug
			'dedit_settings_section_id', // section ID
			array(
				'label_for' => 'dedit_api_key',
				'class' => 'dedit-class', // for <tr> element
			)
		);
		add_settings_field(
			'dedit_blockchain',
			'dedit.io Blockchain Type',
			array($this, 'dedit_blockchain_field_html'),
			'connect-dedit',
			'dedit_settings_section_id',
			array(
				'label_for' => 'dedit_blockchain',
				'class' => 'blockchain-dedit'
			)
		);
	}
	public function validate_dedit_api($api_key)
	{
		$is_valid_api = wp_remote_post('https://enterprise.dedit.io/registration', array(
			'method' => 'POST',
			'headers' => array(
				'X-Api-Key' =>  $api_key
			)
		));
		$is_valid_api_array = json_decode($is_valid_api['body'], true);
		if ($is_valid_api_array['error'] != 'api key not valid') {
			return true;
		} else {
			return false;
		}
	}
	public function dedit_option_notices()
	{
		if ($_REQUEST['page'] == 'connect-dedit' && $_REQUEST['settings-updated'] == true && (!empty(CONNECT_dedit_API_KEY) || CONNECT_dedit_API_KEY != NULL)) {
			if (!$this->validate_dedit_api(CONNECT_dedit_API_KEY)) {
				echo '<div class="notice notice-error is-dismissible">
                        <p>' . __('API key is not valid.', $this->plugin_text_domain) . '</p>
                     </div>';
			} else {
				echo '<div class="notice notice-success is-dismissible">
               <p>' . __('API key is valid.', $this->plugin_text_domain) . '</p>
		 </div>';
			}
		}
		if ($_REQUEST['page'] == 'connect-dedit-api' && $_GET['deleted_users'] == 'success') {
			echo '<div class="notice notice-success is-dismissible">
                        <p>' . __('Dedit users deleted.', $this->plugin_text_domain) . '</p>
                     </div>';
		} elseif ($_REQUEST['page'] == 'connect-dedit-api' && $_GET['deleted_user'] == 'success') {
			echo '<div class="notice notice-success is-dismissible">
                        <p>' . __('Dedit user deleted.', $this->plugin_text_domain) . '</p>
                     </div>';
		}
	}
	public function dedit_text_field_html()
	{
		printf(
			'<input type="text" id="dedit_api_key" class="regular-text" name="dedit_api_key" value="%s" />',
			esc_attr(CONNECT_dedit_API_KEY)
		);
	}
	public function dedit_blockchain_field_html()
	{
		$option = CONNECT_dedit_chaintype;
		if ($option === 'ETHEREUM') {
			$other_option = 'ALGORAND';
		} else {
			$other_option = 'ETHEREUM';
		}
		printf('<select id="dedit_blockchain" name="dedit_blockchain" >
			<option value="%s">%s</option>
			<option value="%s">%s</option>
		</select>', $option, $option, $other_option, $other_option);
	}
	public function enable_notarization_service()
	{
		$is_dedit_api_enabled = CONNECT_dedit_API_KEY;
		if ((!empty($is_dedit_api_enabled) || $is_dedit_api_enabled != NULL) && $this->validate_dedit_api($is_dedit_api_enabled)) {
			if (class_exists('WPCF7_TagGenerator')) {
				$tag_generator = WPCF7_TagGenerator::get_instance();
				$tag_generator->add(
					'en-notarization',
					__('Enable Notarization', $this->plugin_text_domain),
					array($this, 'generating_notarization_properties')
				);
			}
		}
	}
	public function generating_notarization_properties($contact_form, $args = '')
	{
		$args = wp_parse_args($args, array());
		$type = 'checkbox';


		if ('checkbox' === $type) {
			$description = __("Generate a checkbox to make sure that the user approved the notarization service.", $this->plugin_text_domain);
		}
		//include The control box for notarization checkbox generator
		ob_start();
		include_once(CONNECT_dedit_API_PATH . 'admin/partials/en-notarization.php');
		$en_notarization = ob_get_contents();
		ob_end_clean();
		echo $en_notarization;
	}
	//Add Submenu contains all the registered data via dedit API Key
	public function dedit_registered_users()
	{
		add_menu_page("connect-dedit", __("Connect to dedit.io", $this->load_plugin_textdomain), "manage_options", "connect-dedit", array($this, 'dedit_information_fields'));
		add_submenu_page("connect-dedit", __("dedit.io Account Information", $this->plugin_text_domain), __("dedit.io Account Information", $this->plugin_text_domain), "manage_options", "connect-dedit",  array($this, 'dedit_information_fields'));
	}

	//Second Version of the table
	public function add_plugin_admin_menu()
	{

		$page_hook = add_submenu_page(
			"connect-dedit",
			__('Registered Form Submissions', $this->plugin_text_domain), //page title
			__('Registered Form Submissions', $this->plugin_text_domain), //menu title
			'manage_options', //capability
			$this->plugin_name, //menu_slug,
			array($this, 'load_user_list_table')
		);

		add_action('load-' . $page_hook, array($this, 'load_user_list_table_screen_options'));
	}
	public function load_user_list_table_screen_options()
	{
		$arguments = array(
			'label'		=>	__('Users Per Page', $this->plugin_text_domain),
			'default'	=>	10,
			'option'	=>	'users_per_page'
		);
		add_screen_option('per_page', $arguments);
		require_once CONNECT_dedit_API_PATH . 'includes/class-connect-dedit-api-table.php';
		$this->user_list_table = new User_List_Table($this->plugin_text_domain);
	}
	/*
 * Display the User List Table
 * Callback for the add_users_page() in the add_plugin_admin_menu() method of this class.
 */
	public function load_user_list_table()
	{
		// query, filter, and sort the data
		$this->user_list_table->prepare_items();

		include_once('partials/table-options.php');

		echo '<form id="wpse-list-table-form" method="post">';

		$page  = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRIPPED);
		$paged = filter_input(INPUT_GET, 'paged', FILTER_SANITIZE_NUMBER_INT);

		printf('<input type="hidden" name="page" value="%s" />', $page);
		printf('<input type="hidden" name="paged" value="%d" />', $paged);

		$this->user_list_table->display();

		echo '</form>';
	}
	//Handle Actions on Records From the table
	public function process_dedit_records()
	{
		global $wpdb;
		$url = admin_url('admin.php?page=connect-dedit-api');
		check_ajax_referer('dedit-connect-admin', 'security');
		$record_id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
		$wpdb_table = $this->table_activator->dedit_users_data();
		if ($_REQUEST['param'] === 'delete_dedit_record' && $record_id > 0) {
			$wpdb->delete(
				$wpdb_table,
				array(
					'data_id' => $record_id
				)
			);

			echo wp_json_encode(array(
				'admin_url' => $url
			));
		} elseif ($_REQUEST['param'] === 'bulk_delete_users') {
			$ids = []; //catch the ids of the bulk action
			$request_array = (array) $_REQUEST;
			foreach ($request_array as $key => $v) {
				$key_id = explode('-', $key);
				if (in_array('id', $key_id)) {
					$ids[] = $v;
				}
			}
			if (!empty($ids) && $ids != NULL) {
				for ($i = 0; $i < count($ids); $i++) {
					$wpdb->delete(
						$wpdb_table,
						array(
							'data_id' => $ids[$i]
						)
					);
				}
				echo wp_json_encode(array(
					'admin_url' => $url
				));
			}
		} elseif ($_REQUEST['param'] === 'download_dedit_record') {
			$file = $wpdb->get_results(
				$wpdb->prepare("SELECT `formData`, `createdAt` FROM " . $wpdb_table . " WHERE data_id = %d", $record_id)
			);
			if (!empty($file) && $file !== NULL) {
				echo	wp_json_encode(array(
					'download' => $file[0],
					'hashed_file' => hash('sha256', $file[0]->formData),
					'created_time' => $file[0]->createdAt,
					'file_name' => date_i18n(
						'Y-m-d-H-i-s',
						current_time($file[0]->createdAt)
					) . "-" . $file[0]->createdAt
				));
			}
		}
	}
}
