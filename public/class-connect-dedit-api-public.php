<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://webw.us
 * @since      1.0.0
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/public
 * @author     We Build Websites LLC <sales@webw.me>
 */
class Connect_Dedit_Api_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	private $table_activator;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		require_once CONNECT_dedit_API_PATH . 'includes/class-connect-dedit-api-activator.php';
		$activator = new Connect_Dedit_Api_Activator();
		$this->table_activator = $activator;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		wp_enqueue_style('dedit-style', plugin_dir_url(__FILE__) . 'css/connect-dedit-api-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_script('jquery');
		if ((!empty(CONNECT_dedit_API_KEY) || CONNECT_dedit_API_KEY != NULL) &&  $this->validate_dedit_api(CONNECT_dedit_API_KEY)) {
			wp_enqueue_script('dedit-public-js', plugin_dir_url(__FILE__) . 'js/connect-dedit-api-public.js', array('jquery'), $this->version, false);
			wp_localize_script('dedit-public-js', 'dedit_connect_ajax', array(
				'name' => "Dedit.io Connection",
				'ajaxurl' => admin_url("admin-ajax.php"),
				'ajax_nonce' => wp_create_nonce('dedit-connect-public'),
				'hash_success' => __('Your hash has been created successfully', $this->plugin_text_domain),
				'registration_link' => __('Registration Link', $this->plugin_text_domain),
				'hashed_file' => __('The Hashed File', $this->plugin_text_domain)
			));
		} else {
			wp_enqueue_script('dedit-public-js', plugin_dir_url(__FILE__) . 'js/connect-dedit-api-empty.js', array('jquery'), $this->version, false);
		}
	}
	public function validate_dedit_api($api_key)
	{
		$is_valid_api = wp_remote_post('https://enterprise.dedit.io/registration', array(
			'method' => 'POST',
			'headers' => array(
				'X-Api-Key' =>  $api_key
			)
		));
		$is_valid_api_array = json_decode($is_valid_api['body'], true);
		if ($is_valid_api_array['error'] != 'api key not valid') {
			return true;
		} else {
			return false;
		}
	}
	public function handle_ajax_requests_dedit()
	{
		global $wpdb;
		check_ajax_referer('dedit-connect-public', 'security');
		$param = isset($_REQUEST['param']) ? $_REQUEST['param'] : "";
		if (!empty($param)) {
			//remove the slashes from the extracted json from js
			$param_json = stripslashes($param);
			//convert json to a associative array
			$data = json_decode($param_json, true);
			$cf7_data = [];
			//get the cf7 data itself on its own array
			for ($i = 0; $i < 6; $i++) {
				$cf7_data[] = 	array_shift($data);
			}
			//extract form data in a string (data separated by ",")

			$inputs_data = [];
			$array_index = 0;
			for ($i = 0; $i < count($data); $i++) {
				if ($data[$i]['name'] != 'en-notarization[]') {
					$inputs_data[] = $data[$i]['value']; //push it into the array, so that it can be used later
				} else {
					$array_index = $i; //catch the checkbox [notarization acceptance]
				}
			}
			unset($data[$array_index]); //removed it from the data, so that we can deal only with the input form data
			//Assign the json into a variable so that we can send it to the remote server
			$registeration_body =	array(
				"name" => get_the_title($cf7_data[0]['value']), //form titles
				"hash" => hash('sha256', json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)), //hash
				"description" => 'Dedit.io WordPress Notarization Plug-in',
				"blockchainType" => CONNECT_dedit_chaintype,
				"type" => "UNSIGNED"
			);
			$registeration_headers = array(
				'Content-Type' => 'application/json',
				'X-Api-Key' => CONNECT_dedit_API_KEY
			);
			$json_file = $registeration_body;

			//do the actual POST request
			$response = wp_remote_post('https://enterprise.dedit.io/registration', array(
				'method' => 'POST',
				'headers' => $registeration_headers,
				'body' => wp_json_encode([$registeration_body], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
			));
			if (is_wp_error($response)) { //if there is an error with connection
				$error_message = json_decode($response['body'], true);
				echo wp_json_encode($error_message, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
			} else {
				$post_response = json_decode($response['body'], true);
				if (empty($post_response['error']) || $post_response['error'] == NULL) {
					$get_response_array = $post_response[0];
					//user info
					$user_id = $get_response_array['idRegistration'];
					$user_hash = $get_response_array['hash'];
					$user_name = $get_response_array['name'];
					$user_desc = $get_response_array['description'];
					$user_type = $get_response_array['type'];
					$blockchain_type = $get_response_array['blockchainType'];
					$created_user = $get_response_array['createUser'];
					$user_created_date = $get_response_array['createdAt'];
					//insert this data into the DB 
					$wpdb->insert($this->table_activator->dedit_users_data(), array(
						'idRegistration' => $user_id,
						'description' => $user_desc,
						'hash' => $user_hash,
						'name' => $user_name,
						'blockchaintype' => $blockchain_type,
						'signature_type' => $user_type,
						'createUser' => $created_user,
						'createdAt' => $user_created_date,
						'formData' => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),

					));
					if ($wpdb->insert_id > 0) {
						echo wp_json_encode(array(
							//user info
							'idRegistration' => $user_id,
							'hash' => $user_hash,
							'name' => $user_name,
							'description' => $user_desc,
							'type' => $user_type,
							'blockchainType' => $blockchain_type,
							'createUser' => $created_user,
							'createdAt' => date_i18n(
								'Y-m-d-H-i-s',
								current_time($user_created_date)
							) . "-" . $user_created_date,
							'jsonFile' => [$registeration_body],
							'hashed_file' => hash('sha256', wp_json_encode([$registeration_body])),
							'requested_file' => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
						));
					}
				} else {
					echo wp_json_encode(array(
						'error' => $post_response['error']
					));
				}
			}
		}

		wp_die();
	}
	//get the downloadable JSON file from the database for the old form data
	public function get_download_file_using_hash()
	{
		global $wpdb;
		check_ajax_referer('dedit-connect-public', 'security');
		$wpdb_table = $this->table_activator->dedit_users_data();
		if ($_REQUEST['file'] === 'get_dedit_data') {
			$file = $wpdb->get_results(
				$wpdb->prepare("SELECT `formData`, `createdAt` FROM " . $wpdb_table . " WHERE hash = %s", $_REQUEST['hash'])
			);
			if (!empty($file) && $file !== NULL) {
				echo	wp_json_encode(array(
					'download' => $file[0],
					'hashed_file' => hash('sha256', $file[0]->formData),
					'created_time' => $file[0]->createdAt,
					'file_name' => date_i18n(
						'Y-m-d-H-i-s',
						current_time($file[0]->createdAt)
					)   . "-" .  $file[0]->createdAt
				));
			}
		}
	}
}
