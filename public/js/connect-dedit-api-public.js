jQuery(document).ready(function () {
  //Warp checkbox on a label
  jQuery(".wpcf7-list-item").wrap('<label class="check-wrapper"></label>');
  jQuery(document).on("submit", ".wpcf7-form", function () {
    // The data to pass along with the Ajax request
    var passed_data = jQuery(this).serialize();
    var postData = JSON.stringify(jQuery(this).serializeArray());
    if (postData.indexOf("en-notarization") > -1) {
      var special_id = "dedit-" + Math.ceil(Math.random() * 1000); //to prevent the conflict of the multiple forms on the same page
      jQuery(this).append('<div id="' + special_id + '" class="dedit"></div>');
      // Do the actual Ajax request
      passed_data +=
        "&action=dedit_ajax_request&param=" +
        postData +
        "&security=" +
        dedit_connect_ajax.ajax_nonce;
      jQuery.post(dedit_connect_ajax.ajaxurl, passed_data, function (response) {
        var user_data = JSON.parse(response);
        if (typeof user_data.hash != "undefined") {
          jQuery("#" + special_id)
            .html(
              "<strong>" +
                dedit_connect_ajax.hash_success +
                "</strong> " +
                user_data.hash + //user_data.hashed_file
                " <br>" +
                " <a target='_blank' href='https://enterprise.dedit.io/registration/hash/" +
                user_data.hash +
                "'>" +
                dedit_connect_ajax.registration_link +
                "</a>" +
                "<br>"
            )
            .addClass("dedit-success");
          //make a download file
          const file = new Blob([user_data.requested_file], {
            //user_data.jsonFile
            type: "application/json",
          });
          const fileURL = URL.createObjectURL(file);
          // create the link
          const linkElement = document.createElement("a");

          // add the file url
          linkElement.setAttribute("href", fileURL);
          // add the download attribute with name suggestion
          linkElement.setAttribute("download", user_data.createdAt); //user_data.hashed_file
          linkElement.innerHTML = dedit_connect_ajax.hashed_file;
          // add it to the DOM
          jQuery("#" + special_id).append(linkElement);
        } else {
          if (user_data.error.indexOf("found a duplicate registration") > -1) {
            var old_reg = JSON.parse(user_data.error.slice(64)); //get this registration info
            //get the file from db
            var oldHash = old_reg.hash,
              get_data =
                "&action=get_dedit_file&file=get_dedit_data&hash=" +
                oldHash +
                "&security=" +
                dedit_connect_ajax.ajax_nonce;
            jQuery.post(dedit_connect_ajax.ajaxurl, get_data, function (
              response
            ) {
              //handle the response to be downloaded as json file

              var data = response.slice(0, -1),
                user_data = JSON.parse(data);
              const file = new Blob([user_data.download.formData], {
                type: "application/json",
              });
              const fileURL = URL.createObjectURL(file);
              // create the link
              const linkElement = document.createElement("a");

              // add the file url
              linkElement.setAttribute("href", fileURL);

              var fileName = JSON.parse(user_data.download.formData);
    
              // add the download attribute with name suggestion
              linkElement.setAttribute("download", user_data.file_name); //user_data.hashed_file //oldHash
              linkElement.innerHTML = dedit_connect_ajax.hashed_file; //should be translatable

              jQuery("#" + special_id)
                .html(
                  "<strong>" +
                    dedit_connect_ajax.hash_success +
                    "</strong> " +
                    oldHash + //user_data.hash
                    " <br>" +
                    " <a target='_blank' href='https://enterprise.dedit.io/registration/hash/" +
                    oldHash +
                    "'>" +
                    dedit_connect_ajax.registration_link +
                    "</a>" +
                    "<br>"
                )
                .addClass("dedit-success");
              jQuery("#" + special_id).append(linkElement);
            });
          }
        }
      });
    }
  });
});
