<?php

if (!class_exists('Link_List_Table')) {

    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class for displaying registered WordPress Users
 * in a WordPress-like Admin Table with row actions to 
 * perform user meta opeations
 */
class User_List_Table extends WP_List_Table
{
    private $table_activator;

    public function __construct()
    {

        parent::__construct(array(

            'singular'  =>  __('User', $this->plugin_text_domain),    //singular name of the listed records

            'plural'    => __('Users', $this->plugin_text_domain),    //plural name of the listed records

            'ajax'      => true

        ));
        $activator = new Connect_Dedit_Api_Activator();
        $this->table_activator = $activator;
    }
    // just the barebone implementation.
    public function get_columns()
    {
        $table_columns = array(
            'cb'        => '<input type="checkbox" />', // to display the checkbox.	
            'data_id'    =>  __('ID', $this->plugin_text_domain),
            'idRegistration'    =>  __('Registration ID', $this->plugin_text_domain),
            'description'    => __('Description', $this->plugin_text_domain),
            'hash' => __('Hash', $this->plugin_text_domain),
            'name'        => __('Name', $this->plugin_text_domain),
            'blockchaintype' =>  __('Blockchain Type', $this->plugin_text_domain),
            'signature_type' => __('Type', $this->plugin_text_domain),
            'createUser' => __('Created User', $this->plugin_text_domain),
            'createdAt' => __('Created At', $this->plugin_text_domain),
            'download' =>   __('Download', $this->plugin_text_domain),
            'actions' =>   __('Actions', $this->plugin_text_domain),

        );
        return $table_columns;
    }
    public function no_items()
    {
        _e(
            'No Data avaliable.',
            $this->plugin_text_domain
        );
    }
    public function prepare_items()
    {

        // check if a search was performed.
        $user_search_key = isset($_REQUEST['s']) ? $_REQUEST['s'] : '';

        $this->_column_headers = $this->get_column_info();

        // check and process any actions such as bulk actions.
        $this->handle_table_actions();
        //used by WordPress to build and fetch the _column_headers property
        $this->_column_headers = $this->get_column_info();
        $table_data = $this->fetch_table_data();

        // fetch the table data
        $table_data = $this->fetch_table_data();
        // filter the data in case of a search
        if (!empty($user_search_key) || $user_search_key != NULL) {
            $table_data = $this->filter_table_data($table_data, $user_search_key);
        }



        // start by assigning your data to the items variable
        $this->items = $table_data;
        $users_per_page = $this->get_items_per_page('users_per_page');
        $table_page = $this->get_pagenum();

        // provide the ordered data to the List Table
        // we need to manually slice the data based on the current pagination
        $this->items = array_slice($table_data, (($table_page - 1) * $users_per_page), $users_per_page);

        // code to handle pagination
        $total_users = count($table_data);
        $this->set_pagination_args(array(
            'total_items' => $total_users,
            'per_page'    => $users_per_page,
            'total_pages' => ceil($total_users / $users_per_page)
        ));
        $columns  = $this->get_columns();
        $sortable = $this->get_sortable_columns();
        $hidden   = array();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();
    }
    public function fetch_table_data()
    {
        global $wpdb;
        $wpdb_table = $this->table_activator->dedit_users_data();
        $orderby = (isset($_GET['orderby'])) ? esc_sql($_GET['orderby']) : 'data_id';
        $order = (isset($_GET['order'])) ? esc_sql($_GET['order']) : 'DESC';

        $user_query = "SELECT 
                        *
                      FROM 
                        $wpdb_table 
                      ORDER BY $orderby $order";

        // query output_type will be an associative array with ARRAY_A.
        $query_results = $wpdb->get_results($user_query, ARRAY_A);

        // return result array to prepare_items.
        return $query_results;
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'createdAt':
                return date_i18n(get_option('date_format'), current_time($item[$column_name])) . " At " . date_i18n(get_option('time_format'), current_time($item[$column_name]));
            default:
                return $item[$column_name];
        }
    }
    protected function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id-%d" value="%s" />',
            rand(),
            $item['data_id']
        );
    }
    protected function column_actions($item)
    {
        return sprintf('<span class="button button-secondary" id="delete-dedit-record" data-id="%s">%s</span>', $item['data_id'], __('Delete', $this->plugin_text_domain));
    }
    protected function column_download($item)
    {
        return sprintf('<span class="button button-secondary" id="download-dedit-record" data-id="%s">%s</span>', $item['data_id'], __('Download', $this->plugin_text_domain));
    }
    protected function get_sortable_columns()
    {
        /*
	 * actual sorting still needs to be done by prepare_items.
	 * specify which columns should have the sort icon.	
	 */

        $sortable_columns = array(
            'data_id' => ['data_id', true]
        );
        return $sortable_columns;
    }
    // filter the table data based on the search key
    public function filter_table_data($table_data,  $search_key)
    {
        $filtered_table_data = array_values(array_filter($table_data, function ($row) use ($search_key) {
            foreach ($row as $row_val) {
                if (stripos($row_val, $search_key) !== false) {
                    return true;
                }
            }
        }));
        return $filtered_table_data;
    }
    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     *
     * @return string
     */
    public function column_name($item)
    {

        return sprintf('%1$s %2$s', $item['name'], $this->row_actions($actions));
    }
    public function get_bulk_actions()
    {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }
}
