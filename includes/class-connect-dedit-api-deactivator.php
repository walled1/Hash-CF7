<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://webw.us
 * @since      1.0.0
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/includes
 * @author     We Build Websites LLC <sales@webw.me>
 */
class Connect_Dedit_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
