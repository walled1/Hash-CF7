<?php

/**
 * Fired during plugin activation
 *
 * @link       https://webw.us
 * @since      1.0.0
 *
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Connect_Dedit_Api
 * @subpackage Connect_Dedit_Api/includes
 * @author     We Build Websites LLC <sales@webw.me>
 */
class Connect_Dedit_Api_Activator
{

  public function dedit_users_data()
  {
    global $wpdb;
    return $wpdb->prefix . "dedit_notarization_form_data";
  }
  public function activate()
  {
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $dedit_users_table_query = "CREATE TABLE IF NOT EXISTS `" . $wpdb->dbname . "`.`" . $this->dedit_users_data() . "`( 
	`data_id` INT(11) NOT NULL AUTO_INCREMENT,
    `idRegistration` TEXT,
    `description` TEXT,
    `hash` TEXT,
    `name` VARCHAR(255),
    `blockchaintype` VARCHAR(255),
    `signature_type` VARCHAR(255),
    `createUser` TEXT,
    `createdAt` TEXT,
    `formData` JSON,
    PRIMARY KEY(`data_id`)) ENGINE = InnoDB;";
    dbDelta($dedit_users_table_query);
  }
}
