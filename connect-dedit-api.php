<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://webw.us
 * @since             1.0.0
 * @package           Connect_Dedit_Api
 *
 * @wordpress-plugin
 * Plugin Name:       Dedit Form Notarization
 * Plugin URI:        https://webw.us
 * Description:       Dedit Form Notarization.
 * Version:           1.0.0
 * Author:            We Build Websites LLC
 * Author URI:        https://webw.us
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connect-dedit-api
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('CONNECT_DEDIT_API_VERSION', '1.0.0');
define('CONNECT_dedit_API_PATH', plugin_dir_path(__FILE__));
define('CONNECT_dedit_API_URL', plugin_dir_url(__FILE__));
define('CONNECT_dedit_API_KEY', get_option('dedit_api_key'));
define('CONNECT_dedit_chaintype', get_option('dedit_blockchain'));

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-connect-dedit-api-activator.php
 */
function activate_connect_dedit_api()
{
	require_once CONNECT_dedit_API_PATH . 'includes/class-connect-dedit-api-activator.php';
	$activator = new Connect_Dedit_Api_Activator();
	$activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-connect-dedit-api-deactivator.php
 */
function deactivate_connect_dedit_api()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-connect-dedit-api-deactivator.php';
	Connect_Dedit_Api_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_connect_dedit_api');
register_deactivation_hook(__FILE__, 'deactivate_connect_dedit_api');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-connect-dedit-api.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_connect_dedit_api()
{

	$plugin = new Connect_Dedit_Api();
	$plugin->run();
}
run_connect_dedit_api();
